---
date: 2021-12-15
title: Tasks in December 2021
service: overall
---

## Publish [HIFIS Survey 2021]({% link services/overall/survey.html %}) Report
Publish results of HIFIS survey 2021.
