---
date: 2019-12-01
title: Tasks in December 2019
service: software
---

## Initial launch of software.hifis.net
The platform software.hifis.net (meanwhile moved to [hifis.net](https://hifis.net)) is launched as
the future home for software development services within Helmholtz and HIFIS.
The static site is built with Jekyll.
Its sources are available on
[GitLab](https://gitlab.hzdr.de/hifis/software.hifis.net).
Feel free to build upon it.
