---
date: 2021-02-01
title: Tasks in February 2021
service: software
---

## Helmholtz-wide GitLab on new Ansible-managed Infrastructure
The [GitLab service](https://gitlab.hzdr.de) will be deployed to a
new [Ansible-managed infrastructure](https://gitlab.hzdr.de/hifis-software-deployment/gitlab)
providing an enhanced maintainability and scalability.
Access to this Helmholtz-wide GitLab will be granted to all people affiliated
with the Helmholtz Association and partners to foster cross-center
collaboration.
If users encounter any issues, they can report them via
[the HIFIS helpdesk](mailto:support@hifis.net?subject=%5BGitLab%5D%3A%20Add%20Request%20description).
