image: ruby:3.0

stages:
  - build
  - test
  - review
  - accessibility
  - deploy

.artifacts_extension:
  artifacts:
    paths:
      - public
    expire_in: 1d

.cache_latest: &cache_latest
  cache:
    key: latest
    paths:
      - vendor/bundle
      - assets/img/jumbotrons

.cache_production: &cache_production
  cache:
    key: production
    paths:
      - vendor/bundle
      - assets/img/jumbotrons

.common_production: &production_before_script
  before_script:
    - "[[ -x \"$(command -v apt-get)\" ]] && \
          apt-get update && \
          apt-get install -y ffmpeg"
    - wget https://gitlab.hzdr.de/hifis/overall/communication/hifis-teaser-video/-/jobs/artifacts/main/raw/video/video_full.mp4?job=video_build -O assets/videos/video_full.mp4
    - gem install bundler --no-document
    - bundle config set without 'test'
    - bundle install --jobs $(nproc)
  <<: *cache_production

.common_latest: &latest_before_script
  before_script:
    - "[[ -x \"$(command -v apt-get)\" ]] && \
          apt-get update && \
          apt-get install -y ffmpeg"
    - "[[ -f \"Gemfile.lock\" ]] && rm Gemfile.lock"
    - bundle config set without 'test'
    - bundle install --jobs $(nproc) --without test "${FLAGS[@]}"
    - bundle update --jobs $(nproc) "${FLAGS[@]}"
  <<: *cache_latest

.common_variables: &common_variables
  GIT_SUBMODULE_STRATEGY: recursive
  SUBPATH: "/"
  BUNDLE_PATH: vendor/bundle
  NOKOGIRI_USE_SYSTEM_LIBRARIES: "1"

.builds: &builds
  stage: build
  variables:
    <<: *common_variables
  script:
    - "bash scripts/create_jumbotrons.sh assets/img/jumbotrons/"
    - "bundle exec jekyll build --future \
        --config _config.yml \
        -d public$SUBPATH \
        --baseurl $SUBPATH"
    - "cp _favicons/* public/$SUBPATH"
  # https://docs.gitlab.com/ce/ci/yaml/#interruptible
  interruptible: true

.builds_reviewapps: &builds_reviewapps
  stage: build
  variables:
    <<: *common_variables
  script:
    - "bash scripts/create_jumbotrons.sh assets/img/jumbotrons/"
    - "bundle exec jekyll build --future \
        --config _config.yml,_config_reviewapps.yml \
        -d public$SUBPATH \
        --baseurl $SUBPATH"
    - "cp _favicons/* public/$SUBPATH"
  # https://docs.gitlab.com/ce/ci/yaml/#interruptible
  interruptible: true

.tests: &tests
  stage: test
  variables:
    <<: *common_variables
  before_script:
    # Installing, generating and exporting locale because htmlproofer needs it.
    # Locale is not set in "standard" docker containers.
    - "[[ -x \"$(command -v apt-get)\" ]] && \
      apt-get update && \
      apt-get install -y locales && \
      echo \"en_US UTF-8\" > /etc/locale.gen && \
      locale-gen en_US.UTF-8 && \
      export LANG=en_US.UTF-8 && \
      export LANGUAGE=en_US:en && \
      export LC_ALL=en_US.UTF-8"
    - gem install bundler --no-document
    - bundle install --jobs $(nproc)
  script:
    - "bundle exec htmlproofer \
        --internal-domains localhost:4000 \
        --disable-external \
        --assume-extension public/"
    # Use yaml-lint tool to check yaml files.
    - "bundle exec yaml-lint _data/ _config.yml"
  <<: *cache_production
  # https://docs.gitlab.com/ce/ci/yaml/#interruptible
  interruptible: true

# Job runs jekyll build with latest dependencies.
build:latest:
  <<: *builds
  <<: *latest_before_script
  image: ruby:latest
  allow_failure: true

# Job runs jekyll build with production dependencies,
# puts everything in a sub-directory as required for review apps.
build:production:review-apps:
  <<: *builds_reviewapps
  <<: *production_before_script
  variables:
    <<: *common_variables
    SUBPATH: /review-apps/$CI_PROJECT_PATH_SLUG/$CI_COMMIT_REF_SLUG
  extends:
    - .artifacts_extension

# Job runs jekyll build with current production dependencies
build:production:
  <<: *builds
  <<: *production_before_script
  extends:
    - .artifacts_extension

# Job runs htmlproofer basing on production build.
test:htmlproofer:
  <<: *tests
  dependencies:
    - build:production
  needs: ["build:production"]

# Job runs htmlproofer basing on production build with subpath.
test:htmlproofer:subpath:
  <<: *tests
  dependencies:
    - build:production:review-apps
  needs: ["build:production:review-apps"]

# Job tests external links in addition to test:htmlproofer job.
test:htmlproofer:external_links:
  <<: *tests
  dependencies:
    - build:production
  needs: ["build:production"]
  script:
    - "bundle exec htmlproofer \
        --internal-domains localhost:4000 \
        --url-ignore '/www.openstoragenetwork.org/,/www.hzdr.de/,/indico4.twgrid.org/,/webfts.fedcloud-tf.fedcloud.eu/' \
        --assume-extension public/"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
  tags:
    - internal
  allow_failure: true

review:
  stage: review
  variables:
    GIT_STRATEGY: none
    REVIEW_APPS_PATH: review-apps/$CI_PROJECT_PATH_SLUG/$CI_COMMIT_REF_SLUG
  dependencies:
    - build:production:review-apps
  needs: ["build:production:review-apps"]
  script:
    - mkdir -p /var/www/html/gl-review-apps/$REVIEW_APPS_PATH
    - rsync -a --delete ./public/$REVIEW_APPS_PATH/. /var/www/html/gl-review-apps/$REVIEW_APPS_PATH/.
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$APPS_DOMAIN/review-apps/$CI_PROJECT_PATH_SLUG/$CI_COMMIT_REF_SLUG
    on_stop: stop_review
  tags:
    - review-apps

stop_review:
  stage: review
  variables:
    GIT_STRATEGY: none
    REVIEW_APPS_PATH: review-apps/$CI_PROJECT_PATH_SLUG/$CI_COMMIT_REF_SLUG
  script:
    - rm -rf public /var/www/html/gl-review-apps/$REVIEW_APPS_PATH
    - rmdir --ignore-fail-on-non-empty /var/www/html/gl-review-apps/review-apps/$CI_PROJECT_PATH_SLUG/
  dependencies: []
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  tags:
    - review-apps

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - template: 'Verify/Accessibility.gitlab-ci.yml'
  - local: '/.gitlab/ci/a11y.gitlab-ci.yml'
