---
title: "The Earth and Environment DataHub use nubes"
title_image: background-cloud.jpg
date: 2021-06-27
authors:
  - spicker
layout: blogpost
categories:
  - Use Case
excerpt: >
  The Earth and Environment DataHub was one of the first project groups to use nubes as a sync+share service of the Helmholtz Cloud.
  Since then, the number of user groups has been growing steadily.

---

### The Earth and Environment DataHub use nubes, a Helmholtz Cloud Sync+Share Service

The [Earth and Environment DataHub](https://www.helmholtz.de/en/research/research-fields/earth-and-environment/)
was one of the first project groups to use nubes as a sync+share service of the [Helmholtz Cloud](https://helmholtz.cloud).

"nubes provides a central sharing platform for our DataHub Team with more than 100 researchers,
IT-experts and stakeholders across 7 Helmholtz Centres.
With its simple user management and Helmholtz-wide login,
it is an essential element towards an interlinked research and collaboration infrastructure for the DataHub and beyond."
says Sabine Barthlott, DataHub manager.

### nubes by Helmholtz-Zentrum Berlin

Share files and documents Helmholtz-wide without additional passwords,
links or login data: With the [Helmholtz AAI](https://aai.helmholtz.de/),
using the Sync&Share services in the Helmholtz Cloud becomes easier than ever.
The bureaucratic and technical hurdles are low for users.
This is ensured by the agreements that HIFIS is currently organising at a higher level.
nubes became a pilot service of the Helmholtz Cloud in summer 2020.
Since then, the number of users increase constantly.

nubes is a sync+share service based on Nextcloud.
Here, group folders are made available on a project basis.
Unlike many other sync&share cloud services,
the data belongs to a project group instead of a single person -
an advantage that can quickly become essential.
Thus, it is no surprise that more and more Helmholtz-wide projects and platforms are using nubes.
A prominent example is the Earth and Environment DataHub.

### The Earth and Environment DataHub

The Earth and Environment DataHub has three decentralised Hubs (ATMO, MARE, TERRA) that form an interconnected
base structure and are also interlinked by well-defined working groups.
The creation and sustainable operation of the DataHub as a FAIR-ready infrastructure provides data science in
Earth and Environment a solid foundation for new insights.

#### Interested? Need help? Comments and Suggestions?

If you are interested in using nubes as a Sync+Share service for your Helmholtz project,
please do not hesitate to contact us:

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>

