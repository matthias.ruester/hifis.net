---
title: HIFIS Transfer Service
title_image: globe.jpeg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
excerpt:
  "An FTS based large data transfer service connecting Helmholtz research."
redirect_to: news/2021/05/05/hifis-transfer-service.html
---
